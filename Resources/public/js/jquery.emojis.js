(function(window,$){

    $.fn.atm_emojis = function(options){

        var options = options;
        var blue = ['blue-1','blue-2','blue-3','blue-4','blue-5','blue-6','blue-7','blue-8','blue-9','blue-10','blue-11','blue-12','blue-13','blue-14','blue-15','blue-16','blue-17','blue-18','blue-19','blue-20','blue-21','blue-22','blue-23','blue-24','blue-25','blue-26','blue-27','blue-28','blue-29','blue-30','blue-31','blue-32','blue-33','blue-34','blue-35','blue-36','blue-37','blue-38','blue-39','blue-40','blue-41','blue-42','blue-43','blue-44','blue-45','blue-46','blue-47','blue-48','blue-49','blue-50','blue-51','blue-52','blue-53','blue-54','blue-55','blue-56','blue-57','blue-58','blue-59','blue-60','blue-61','blue-62','blue-63','blue-64','blue-65','blue-66','blue-67','blue-68','blue-69','blue-70','blue-71','blue-72','blue-73','blue-74','blue-75','blue-76','blue-77','blue-78','blue-79','blue-80','blue-81','blue-82','blue-83','blue-84','blue-85','blue-86','blue-87','blue-88','blue-89','blue-90','blue-91','blue-92','blue-93','blue-94','blue-95','blue-96','blue-97','blue-98','blue-99','blue-100','blue-101','blue-102','blue-103','blue-104','blue-105','blue-106','blue-107','blue-108','blue-109','blue-110','blue-111','blue-112','blue-113','blue-114','blue-115','blue-116','blue-117','blue-118','blue-119','blue-120','blue-121','blue-122','blue-123','blue-124','blue-125','blue-126','blue-127','blue-128','blue-129','blue-130','blue-131','blue-132','blue-133','blue-134','blue-135','blue-136','blue-137','blue-138','blue-139','blue-140','blue-141','blue-142','blue-143','blue-144','blue-145','blue-146','blue-147','blue-148','blue-149','blue-150','blue-151','blue-152','blue-153','blue-154','blue-155','blue-156','blue-157','blue-158','blue-159','blue-160','blue-161','blue-162','blue-163','blue-164','blue-165','blue-166','blue-167','blue-168','blue-169','blue-170','blue-171','blue-172','blue-173','blue-174','blue-175','blue-176','blue-177','blue-178','blue-179','blue-180','blue-181','blue-182','blue-183','blue-184','blue-185','blue-186','blue-187','blue-188','blue-189','blue-190','blue-191','blue-192','blue-193','blue-194','blue-195','blue-196','blue-197','blue-198','blue-199','blue-200','blue-201','blue-202','blue-203','blue-204','blue-205','blue-206','blue-207','blue-208','blue-209','blue-210','blue-211','blue-212','blue-213','blue-214','blue-215','blue-216','blue-217','blue-218','blue-219','blue-220','blue-221','blue-222','blue-223','blue-224','blue-225','blue-226','blue-227','blue-228','blue-229','blue-230','blue-231','blue-232','blue-233','blue-234','blue-235','blue-236','blue-237','blue-238','blue-239','blue-240','blue-241','blue-242','blue-243','blue-244','blue-245','blue-246','blue-247','blue-248','blue-249','blue-250','blue-251','blue-252','blue-253','blue-254','blue-255','blue-256','blue-257','blue-258','blue-259','blue-260','blue-261','blue-262','blue-263','blue-264','blue-265','blue-266','blue-267','blue-268','blue-269','blue-270','blue-271','blue-272','blue-273','blue-274','blue-275','blue-276','blue-277','blue-278','blue-279','blue-280','blue-281','blue-282','blue-283','blue-284','blue-285','blue-286','blue-287','blue-288','blue-289','blue-290','blue-291','blue-292','blue-293','blue-294','blue-295','blue-296','blue-297','blue-298','blue-299','blue-300','blue-301','blue-302','blue-303','blue-304','blue-305','blue-306'];
        var cyan = ['cyan-1','cyan-2','cyan-3','cyan-4','cyan-5','cyan-6','cyan-7','cyan-8','cyan-9','cyan-10','cyan-11','cyan-12','cyan-13','cyan-14','cyan-15','cyan-16','cyan-17','cyan-18','cyan-19','cyan-20','cyan-21','cyan-22','cyan-23','cyan-24','cyan-25','cyan-26','cyan-27','cyan-28','cyan-29','cyan-30','cyan-31','cyan-32','cyan-33','cyan-34','cyan-35','cyan-36','cyan-37','cyan-38','cyan-39','cyan-40','cyan-41','cyan-42','cyan-43','cyan-44','cyan-45','cyan-46','cyan-47','cyan-48','cyan-49','cyan-50','cyan-51','cyan-52','cyan-53','cyan-54','cyan-55','cyan-56','cyan-57','cyan-58','cyan-59','cyan-60','cyan-61','cyan-62','cyan-63','cyan-64','cyan-65','cyan-66','cyan-67','cyan-68','cyan-69','cyan-70','cyan-71','cyan-72','cyan-73','cyan-74','cyan-75','cyan-76','cyan-77','cyan-78','cyan-79','cyan-80','cyan-81','cyan-82','cyan-83','cyan-84','cyan-85','cyan-86','cyan-87','cyan-88','cyan-89','cyan-90','cyan-91','cyan-92','cyan-93','cyan-94','cyan-95','cyan-96','cyan-97','cyan-98','cyan-99','cyan-100','cyan-101','cyan-102','cyan-103','cyan-104','cyan-105','cyan-106','cyan-107','cyan-108','cyan-109','cyan-110','cyan-111','cyan-112','cyan-113','cyan-114','cyan-115','cyan-116','cyan-117','cyan-118','cyan-119','cyan-120','cyan-121','cyan-122','cyan-123','cyan-124','cyan-125','cyan-126','cyan-127','cyan-128','cyan-129','cyan-130','cyan-131','cyan-132','cyan-133','cyan-134','cyan-135','cyan-136','cyan-137','cyan-138','cyan-139','cyan-140','cyan-141','cyan-142','cyan-143','cyan-144','cyan-145','cyan-146','cyan-147','cyan-148','cyan-149','cyan-150','cyan-151','cyan-152','cyan-153','cyan-154','cyan-155','cyan-156','cyan-157','cyan-158','cyan-159','cyan-160','cyan-161','cyan-162','cyan-163','cyan-164','cyan-165','cyan-166','cyan-167','cyan-168','cyan-169','cyan-170','cyan-171','cyan-172','cyan-173','cyan-174','cyan-175','cyan-176','cyan-177','cyan-178','cyan-179','cyan-180','cyan-181','cyan-182','cyan-183','cyan-184','cyan-185','cyan-186','cyan-187','cyan-188','cyan-189','cyan-190','cyan-191','cyan-192','cyan-193','cyan-194','cyan-195','cyan-196','cyan-197','cyan-198','cyan-199','cyan-200','cyan-201','cyan-202','cyan-203','cyan-204','cyan-205','cyan-206','cyan-207','cyan-208','cyan-209','cyan-210','cyan-211','cyan-212','cyan-213','cyan-214','cyan-215','cyan-216','cyan-217','cyan-218','cyan-219','cyan-220','cyan-221','cyan-222','cyan-223','cyan-224','cyan-225','cyan-226','cyan-227','cyan-228','cyan-229','cyan-230','cyan-231','cyan-232','cyan-233','cyan-234','cyan-235','cyan-236','cyan-237','cyan-238','cyan-239','cyan-240','cyan-241','cyan-242','cyan-243','cyan-244','cyan-245','cyan-246','cyan-247','cyan-248','cyan-249','cyan-250','cyan-251','cyan-252','cyan-253','cyan-254','cyan-255','cyan-256','cyan-257','cyan-258','cyan-259','cyan-260','cyan-261','cyan-262','cyan-263','cyan-264','cyan-265','cyan-266','cyan-267','cyan-268','cyan-269','cyan-270','cyan-271','cyan-272','cyan-273','cyan-274','cyan-275','cyan-276','cyan-277','cyan-278','cyan-279','cyan-280','cyan-281','cyan-282','cyan-283','cyan-284','cyan-285','cyan-286','cyan-287','cyan-288','cyan-289','cyan-290','cyan-291','cyan-292','cyan-293','cyan-294','cyan-295','cyan-296','cyan-297','cyan-298','cyan-299','cyan-300','cyan-301','cyan-302','cyan-303','cyan-304','cyan-305','cyan-306'];
        var grey = ['grey-1','grey-2','grey-3','grey-4','grey-5','grey-6','grey-7','grey-8','grey-9','grey-10','grey-11','grey-12','grey-13','grey-14','grey-15','grey-16','grey-17','grey-18','grey-19','grey-20','grey-21','grey-22','grey-23','grey-24','grey-25','grey-26','grey-27','grey-28','grey-29','grey-30','grey-31','grey-32','grey-33','grey-34','grey-35','grey-36','grey-37','grey-38','grey-39','grey-40','grey-41','grey-42','grey-43','grey-44','grey-45','grey-46','grey-47','grey-48','grey-49','grey-50','grey-51','grey-52','grey-53','grey-54','grey-55','grey-56','grey-57','grey-58','grey-59','grey-60','grey-61','grey-62','grey-63','grey-64','grey-65','grey-66','grey-67','grey-68','grey-69','grey-70','grey-71','grey-72','grey-73','grey-74','grey-75','grey-76','grey-77','grey-78','grey-79','grey-80','grey-81','grey-82','grey-83','grey-84','grey-85','grey-86','grey-87','grey-88','grey-89','grey-90','grey-91','grey-92','grey-93','grey-94','grey-95','grey-96','grey-97','grey-98','grey-99','grey-100','grey-101','grey-102','grey-103','grey-104','grey-105','grey-106','grey-107','grey-108','grey-109','grey-110','grey-111','grey-112','grey-113','grey-114','grey-115','grey-116','grey-117','grey-118','grey-119','grey-120','grey-121','grey-122','grey-123','grey-124','grey-125','grey-126','grey-127','grey-128','grey-129','grey-130','grey-131','grey-132','grey-133','grey-134','grey-135','grey-136','grey-137','grey-138','grey-139','grey-140','grey-141','grey-142','grey-143','grey-144','grey-145','grey-146','grey-147','grey-148','grey-149','grey-150','grey-151','grey-152','grey-153','grey-154','grey-155','grey-156','grey-157','grey-158','grey-159','grey-160','grey-161','grey-162','grey-163','grey-164','grey-165','grey-166','grey-167','grey-168','grey-169','grey-170','grey-171','grey-172','grey-173','grey-174','grey-175','grey-176','grey-177','grey-178','grey-179','grey-180','grey-181','grey-182','grey-183','grey-184','grey-185','grey-186','grey-187','grey-188','grey-189','grey-190','grey-191','grey-192','grey-193','grey-194','grey-195','grey-196','grey-197','grey-198','grey-199','grey-200','grey-201','grey-202','grey-203','grey-204','grey-205','grey-206','grey-207','grey-208','grey-209','grey-210','grey-211','grey-212','grey-213','grey-214','grey-215','grey-216','grey-217','grey-218','grey-219','grey-220','grey-221','grey-222','grey-223','grey-224','grey-225','grey-226','grey-227','grey-228','grey-229','grey-230','grey-231','grey-232','grey-233','grey-234','grey-235','grey-236','grey-237','grey-238','grey-239','grey-240','grey-241','grey-242','grey-243','grey-244','grey-245','grey-246','grey-247','grey-248','grey-249','grey-250','grey-251','grey-252','grey-253','grey-254','grey-255','grey-256','grey-257','grey-258','grey-259','grey-260','grey-261','grey-262','grey-263','grey-264','grey-265','grey-266','grey-267','grey-268','grey-269','grey-270','grey-271','grey-272','grey-273','grey-274','grey-275','grey-276','grey-277','grey-278','grey-279','grey-280','grey-281','grey-282','grey-283','grey-284','grey-285','grey-286','grey-287','grey-288','grey-289','grey-290','grey-291','grey-292','grey-293','grey-294','grey-295','grey-296','grey-297','grey-298','grey-299','grey-300','grey-301','grey-302','grey-303','grey-304','grey-305','grey-306'];
        var pink = ['pink-1','pink-2','pink-3','pink-4','pink-5','pink-6','pink-7','pink-8','pink-9','pink-10','pink-11','pink-12','pink-13','pink-14','pink-15','pink-16','pink-17','pink-18','pink-19','pink-20','pink-21','pink-22','pink-23','pink-24','pink-25','pink-26','pink-27','pink-28','pink-29','pink-30','pink-31','pink-32','pink-33','pink-34','pink-35','pink-36','pink-37','pink-38','pink-39','pink-40','pink-41','pink-42','pink-43','pink-44','pink-45','pink-46','pink-47','pink-48','pink-49','pink-50','pink-51','pink-52','pink-53','pink-54','pink-55','pink-56','pink-57','pink-58','pink-59','pink-60','pink-61','pink-62','pink-63','pink-64','pink-65','pink-66','pink-67','pink-68','pink-69','pink-70','pink-71','pink-72','pink-73','pink-74','pink-75','pink-76','pink-77','pink-78','pink-79','pink-80','pink-81','pink-82','pink-83','pink-84','pink-85','pink-86','pink-87','pink-88','pink-89','pink-90','pink-91','pink-92','pink-93','pink-94','pink-95','pink-96','pink-97','pink-98','pink-99','pink-100','pink-101','pink-102','pink-103','pink-104','pink-105','pink-106','pink-107','pink-108','pink-109','pink-110','pink-111','pink-112','pink-113','pink-114','pink-115','pink-116','pink-117','pink-118','pink-119','pink-120','pink-121','pink-122','pink-123','pink-124','pink-125','pink-126','pink-127','pink-128','pink-129','pink-130','pink-131','pink-132','pink-133','pink-134','pink-135','pink-136','pink-137','pink-138','pink-139','pink-140','pink-141','pink-142','pink-143','pink-144','pink-145','pink-146','pink-147','pink-148','pink-149','pink-150','pink-151','pink-152','pink-153','pink-154','pink-155','pink-156','pink-157','pink-158','pink-159','pink-160','pink-161','pink-162','pink-163','pink-164','pink-165','pink-166','pink-167','pink-168','pink-169','pink-170','pink-171','pink-172','pink-173','pink-174','pink-175','pink-176','pink-177','pink-178','pink-179','pink-180','pink-181','pink-182','pink-183','pink-184','pink-185','pink-186','pink-187','pink-188','pink-189','pink-190','pink-191','pink-192','pink-193','pink-194','pink-195','pink-196','pink-197','pink-198','pink-199','pink-200','pink-201','pink-202','pink-203','pink-204','pink-205','pink-206','pink-207','pink-208','pink-209','pink-210','pink-211','pink-212','pink-213','pink-214','pink-215','pink-216','pink-217','pink-218','pink-219','pink-220','pink-221','pink-222','pink-223','pink-224','pink-225','pink-226','pink-227','pink-228','pink-229','pink-230','pink-231','pink-232','pink-233','pink-234','pink-235','pink-236','pink-237','pink-238','pink-239','pink-240','pink-241','pink-242','pink-243','pink-244','pink-245','pink-246','pink-247','pink-248','pink-249','pink-250','pink-251','pink-252','pink-253','pink-254','pink-255','pink-256','pink-257','pink-258','pink-259','pink-260','pink-261','pink-262','pink-263','pink-264','pink-265','pink-266','pink-267','pink-268','pink-269','pink-270','pink-271','pink-272','pink-273','pink-274','pink-275','pink-276','pink-277','pink-278','pink-279','pink-280','pink-281','pink-282','pink-283','pink-284','pink-285','pink-286','pink-287','pink-288','pink-289','pink-290','pink-291','pink-292','pink-293','pink-294','pink-295','pink-296','pink-297','pink-298','pink-299','pink-300','pink-301','pink-302','pink-303','pink-304','pink-305','pink-306'];
        var rose = ['rose-1','rose-2','rose-3','rose-4','rose-5','rose-6','rose-7','rose-8','rose-9','rose-10','rose-11','rose-12','rose-13','rose-14','rose-15','rose-16','rose-17','rose-18','rose-19','rose-20','rose-21','rose-22','rose-23','rose-24','rose-25','rose-26','rose-27','rose-28','rose-29','rose-30','rose-31','rose-32','rose-33','rose-34','rose-35','rose-36','rose-37','rose-38','rose-39','rose-40','rose-41','rose-42','rose-43','rose-44','rose-45','rose-46','rose-47','rose-48','rose-49','rose-50','rose-51','rose-52','rose-53','rose-54','rose-55','rose-56','rose-57','rose-58','rose-59','rose-60','rose-61','rose-62','rose-63','rose-64','rose-65','rose-66','rose-67','rose-68','rose-69','rose-70','rose-71','rose-72','rose-73','rose-74','rose-75','rose-76','rose-77','rose-78','rose-79','rose-80','rose-81','rose-82','rose-83','rose-84','rose-85','rose-86','rose-87','rose-88','rose-89','rose-90','rose-91','rose-92','rose-93','rose-94','rose-95','rose-96','rose-97','rose-98','rose-99','rose-100','rose-101','rose-102','rose-103','rose-104','rose-105','rose-106','rose-107','rose-108','rose-109','rose-110','rose-111','rose-112','rose-113','rose-114','rose-115','rose-116','rose-117','rose-118','rose-119','rose-120','rose-121','rose-122','rose-123','rose-124','rose-125','rose-126','rose-127','rose-128','rose-129','rose-130','rose-131','rose-132','rose-133','rose-134','rose-135','rose-136','rose-137','rose-138','rose-139','rose-140','rose-141','rose-142','rose-143','rose-144','rose-145','rose-146','rose-147','rose-148','rose-149','rose-150','rose-151','rose-152','rose-153','rose-154','rose-155','rose-156','rose-157','rose-158','rose-159','rose-160','rose-161','rose-162','rose-163','rose-164','rose-165','rose-166','rose-167','rose-168','rose-169','rose-170','rose-171','rose-172','rose-173','rose-174','rose-175','rose-176','rose-177','rose-178','rose-179','rose-180','rose-181','rose-182','rose-183','rose-184','rose-185','rose-186','rose-187','rose-188','rose-189','rose-190','rose-191','rose-192','rose-193','rose-194','rose-195','rose-196','rose-197','rose-198','rose-199','rose-200','rose-201','rose-202','rose-203','rose-204','rose-205','rose-206','rose-207','rose-208','rose-209','rose-210','rose-211','rose-212','rose-213','rose-214','rose-215','rose-216','rose-217','rose-218','rose-219','rose-220','rose-221','rose-222','rose-223','rose-224','rose-225','rose-226','rose-227','rose-228','rose-229','rose-230','rose-231','rose-232','rose-233','rose-234','rose-235','rose-236','rose-237','rose-238','rose-239','rose-240','rose-241','rose-242','rose-243','rose-244','rose-245','rose-246','rose-247','rose-248','rose-249','rose-250','rose-251','rose-252','rose-253','rose-254','rose-255','rose-256','rose-257','rose-258','rose-259','rose-260','rose-261','rose-262','rose-263','rose-264','rose-265','rose-266','rose-267','rose-268','rose-269','rose-270','rose-271','rose-272','rose-273','rose-274','rose-275','rose-276','rose-277','rose-278','rose-279','rose-280','rose-281','rose-282','rose-283','rose-284','rose-285','rose-286','rose-287','rose-288','rose-289','rose-290','rose-291','rose-292','rose-293','rose-294','rose-295','rose-296','rose-297','rose-298','rose-299','rose-300','rose-301','rose-302','rose-303','rose-304','rose-305','rose-306'];
        var silver = ['silver-1','silver-2','silver-3','silver-4','silver-5','silver-6','silver-7','silver-8','silver-9','silver-10','silver-11','silver-12','silver-13','silver-14','silver-15','silver-16','silver-17','silver-18','silver-19','silver-20','silver-21','silver-22','silver-23','silver-24','silver-25','silver-26','silver-27','silver-28','silver-29','silver-30','silver-31','silver-32','silver-33','silver-34','silver-35','silver-36','silver-37','silver-38','silver-39','silver-40','silver-41','silver-42','silver-43','silver-44','silver-45','silver-46','silver-47','silver-48','silver-49','silver-50','silver-51','silver-52','silver-53','silver-54','silver-55','silver-56','silver-57','silver-58','silver-59','silver-60','silver-61','silver-62','silver-63','silver-64','silver-65','silver-66','silver-67','silver-68','silver-69','silver-70','silver-71','silver-72','silver-73','silver-74','silver-75','silver-76','silver-77','silver-78','silver-79','silver-80','silver-81','silver-82','silver-83','silver-84','silver-85','silver-86','silver-87','silver-88','silver-89','silver-90','silver-91','silver-92','silver-93','silver-94','silver-95','silver-96','silver-97','silver-98','silver-99','silver-100','silver-101','silver-102','silver-103','silver-104','silver-105','silver-106','silver-107','silver-108','silver-109','silver-110','silver-111','silver-112','silver-113','silver-114','silver-115','silver-116','silver-117','silver-118','silver-119','silver-120','silver-121','silver-122','silver-123','silver-124','silver-125','silver-126','silver-127','silver-128','silver-129','silver-130','silver-131','silver-132','silver-133','silver-134','silver-135','silver-136','silver-137','silver-138','silver-139','silver-140','silver-141','silver-142','silver-143','silver-144','silver-145','silver-146','silver-147','silver-148','silver-149','silver-150','silver-151','silver-152','silver-153','silver-154','silver-155','silver-156','silver-157','silver-158','silver-159','silver-160','silver-161','silver-162','silver-163','silver-164','silver-165','silver-166','silver-167','silver-168','silver-169','silver-170','silver-171','silver-172','silver-173','silver-174','silver-175','silver-176','silver-177','silver-178','silver-179','silver-180','silver-181','silver-182','silver-183','silver-184','silver-185','silver-186','silver-187','silver-188','silver-189','silver-190','silver-191','silver-192','silver-193','silver-194','silver-195','silver-196','silver-197','silver-198','silver-199','silver-200','silver-201','silver-202','silver-203','silver-204','silver-205','silver-206','silver-207','silver-208','silver-209','silver-210','silver-211','silver-212','silver-213','silver-214','silver-215','silver-216','silver-217','silver-218','silver-219','silver-220','silver-221','silver-222','silver-223','silver-224','silver-225','silver-226','silver-227','silver-228','silver-229','silver-230','silver-231','silver-232','silver-233','silver-234','silver-235','silver-236','silver-237','silver-238','silver-239','silver-240','silver-241','silver-242','silver-243','silver-244','silver-245','silver-246','silver-247','silver-248','silver-249','silver-250','silver-251','silver-252','silver-253','silver-254','silver-255','silver-256','silver-257','silver-258','silver-259','silver-260','silver-261','silver-262','silver-263','silver-264','silver-265','silver-266','silver-267','silver-268','silver-269','silver-270','silver-271','silver-272','silver-273','silver-274','silver-275','silver-276','silver-277','silver-278','silver-279','silver-280','silver-281','silver-282','silver-283','silver-284','silver-285','silver-286','silver-287','silver-288','silver-289','silver-290','silver-291','silver-292','silver-293','silver-294','silver-295','silver-296','silver-297','silver-298','silver-299','silver-300','silver-301','silver-302','silver-303','silver-304','silver-305','silver-306'];
        var yellow = ['grinning-face','grinning-face-with-big-eyes','overpleased-face','grinning-face-with-smiling-eyes','beaming-face-with-smiling-eyes','grinning-squinting-face','upside-down-dizzy-face','rofl-cowboy','face-biting-lip','grinning-face-with-sweat','face-with-tears-of-joy','horns-with-tears-of-joy','face-rolling-eyes-with-halo','smiling-face-with-horns-and-halo','clown-face','slightly-smiling-face','upside-down-face','upside-down-face-with-tongue','winking-face','winking-face-with-halo','winking-face-with-horns','smiling-face-with-heart-eyes','drooling-face-wtih-heart-eyes','grimacing-face-with-heart-eyes','emoji-zany-face-heart-eyes','smiling-face-with-rainbow-heart-eyes','smiling-face-with-hearts','angry-face-three-hearts','face-blowing-a-kiss','heart-kisses-face','face-with-flower-band','kissing-face','kissing-face-with-closed-eyes','kissing-face-with-smiling-eyes','angry-kissing-face','face-with-kiss-marks','girlie-face','face-with-tongue-licking-up','face-savoring-food','face-putting-tongue-out','face-with-tongue','face-sticking-tongue-out-in-disgust','squinting-face-with-tongue','squinting-face-with-tongue-and-horns','winking-face-with-tongue','emoji-zany-face','face-with-raised-eyebrow','emotional-face','face-with-monocle','face-with-magnifying-glass','smiling-face-with-sunglasses','lowered-sunglasses-face','thinking-face-with-sunglasses','grinning-face-with-sunglasses','nerd-face-with-sunglasses','frustrated-nerd-face','nerd-face','face-with-disguise','crying-face-with-glasses','face-with-binoculars','face-with-rainbow-glasses','face-with-uniglasses','face-with-3d-glasses','face-with-ski-goggles','snorkel-face','face-with-moustache','star-struck','lucky-face','starry-delighted-face','partying-face','angry-partying-face','angry-face-with-horns','smirking-face','unamused-face','grumpy-face','confused-face','concerned-face','worried-face','frowning-face','pensive-face','persevering-face','dirty-face','slightly-frowning-face','confounded-face','disappointed-face','angry-astonished-face','angry-face','face-with-evil-grin','pleading-face','weary-face','crying-clown-face','crying-face','sleepy-face','crying-cowboy-face','crying-partying-face','loudly-crying-face','frowning-face-with-two-thumbs-down','pouting-face','pouting-face-without-mouth','vampire-face','face-with-steam-from-nose','bandito-face','exploding-head','face-with-symbols-on-mouth','astonished-face-broken-heart-eyes','exploding-angry-head','flushed-face','face-with-tape-over-mouth','astonished-face','hot-face','cross-eyed-face','drooling-face','fire-mouth-face','cold-face','face-screaming-in-fear','downcast-face-with-sweat','fearful-face','fearful-face-with-tongue-sticking-out','face-with-hands-over-ears','anxious-face-with-sweat','sad-but-relieved-face','hugging-face','jazz-hands','eyes-on-you-face','face-biting-finger-nails','face-with-hands-over-eyes','face-with-hands-over-face','face-with-thought-bubble','face-with-hand-over-mouth','facepalm-face','daydreaming-face','shushing-face','speak-no-evil-face','thinking-face','thumbs-down-face','grinning-squinting-shushing-face','pointing-laughing-face','italian-face','face-with-fist-pump','rock-on-face','call-me-face','face-shouting','talk-to-the-hand-face','crossed-fingers-face','evil-plotting-face','thumbs-up-face','face-giving-the-peace-sign','face-holding-flag','face-nose-picking','saluting-face','whispering-face','face-with-selfie','broken-face','money-bags-face','see-no-evil-face','clapping-face','face-with-tea','face-with-coffee','face-with-beer','face-with-wine-glass','face-with-martini','smiling-face-with-two-thumbs-up','hear-no-evil-face','face-with-fork-and-knife','face-with-braces','face-with-lightbulb','face-with-mohawk','spitting-face','whistling-face','face-with-a-bed-sheets','face-with-beanie','face-with-beard','robot-face','pirate-face','smiling-face-with-headphones','face-with-beret','calm-face-with-headphones','camo-emoji-face','ninja-face','sporty-face','martial-arts-face','fan-hand-face','face-with-dumbbell','face-with-boxing-gloves','megaphone-face','face-with-laptop','face-with-newspaper','dirty-injured-face','smug-face','pie-face','death-face','requesting-emoji-face','lying-face','emoji-zipper-mouth-face','face-without-mouth','expressionless-face','neutral-face','neutral-face-rolling-eyes','face-with-rolling-eyes','woozy-neutral-face','grimacing-face','clenched-teeth-scared-face','face-with-open-mouth','frowning-face-with-open-mouth','hangry-face','hushed-face','dizzy-face','anguished-face','huge-yawn-face','exhausted-face','tired-face','sleeping-face','yawning-sleeping-face','hushed-face-x-eyes','tired-face-x-eyes','woozy-face','mummy-face','nauseated-face','face-vomiting','exploding-nauseated-head','nauseated-partying-face','clown-vomiting-rainbows','face-vomiting-rainbows','tongue-on-the-floor-face','kaput-face','face-with-medical-mask','face-with-head-bandage','face-with-pacifier','sneezing-face','face-with-thermometer','money-mouth-face','cowboy-hat-face','smirking-face-with-horns','smiling-face-with-horns','crying-face-with-horns','z-poo-blowing-kiss','z-poo-crying','z-poo-exploding-head','z-poo-face-with-heart-eyes','z-poo-nerd-face','z-poo-partying','z-poo-pile-of','z-poo-princess','z-poo-sleeping','z-poo-unicorn','z-poo-vomiting-rainbows','z-poo-vomiting','z-poo-with-disguise','z-poo-with-medical-mask','z-poo-with-newspaper','z-poo-with-star-eyes','z-poo-with-sunglasses','z-poo-with-symbols-on-mouth','z-poo-with-tears-of-joy','z-poo-with-zipper-mouth','yellow-16','yellow-17','yellow-18','yellow-19','yellow-20','yellow-21','yellow-22','yellow-23','yellow-24','yellow-25','yellow-26','yellow-27','yellow-28','yellow-29','yellow-30','yellow-32','yellow-33','yellow-39','yellow-7','yellow-10','zzz-cat-blowing-kiss','zzz-cat-crying','zzz-cat-face-with-disguise','zzz-cat-face-with-green-vomit','zzz-cat-face-with-head-bandage','zzz-cat-face-with-rainbow-vomit','zzz-cat-face-with-star-eyes','zzz-cat-face-with-sunglasses','zzz-cat-face-with-symbols-on-mouth','zzz-cat-face-with-zipper-mouth','zzz-cat-face','zzz-cat-grinning-with-smiling-eyes','zzz-cat-grinning','zzz-cat-kissing','zzz-cat-nerd-face','zzz-cat-pouting','zzz-cat-sleeping','zzz-cat-smiling-with-heart-eyes','zzz-cat-weary','zzz-cat-with-tears-of-joy','zzz-cat-with-wry-smile','partying-cat','exploding-cat-head','zombie-crying','zombie-face-blowing-a-kiss','zombie-face-screaming-in-fear','zombie-face-with-black-heart-eyes','zombie-face-with-sunglasses','zombie-face','zombie-nerd-face','zombie-party-face','zombie-vomiting-rainbows','zombie-with-symbols-on-mouth','zombie-with-tears-of-joy','zombie-with-zipper-mouth','grinning-face-with-dizzy-goggles','relieved-face','rolling-on-the-floor-laughing','shy-face','skeptical-face','skeptical-smile-face','smiling-face-with-halo','smiling-face-with-smiling-eyes','smiling-face-with-tear','smiling-face','sobbing-face','stoned-face','suspicious-face','yawning-face','yellow-4','yellow-8','yellow-9','yellow-11','yellow-12','yellow-13','yellow-14','yellow-15','yellow-31','yellow-40','yellow-42','yellow-43','yellow-44','yellow-45','yellow-46','yellow-47','yellow-48','yellow-49','yellow-50','yellow-51','zz-hand-backhand-index-pointing-right','zz-hand-clapping-hands','zz-hand-crossed-fingers','zz-hand-flexed-biceps','zz-hand-folded-hands','zz-hand-love-you-gesture','zz-hand-ok-hand','zz-hand-raised-back-of-hand','zz-hand-raising-hands','zz-hand-thumbs-down','zz-hand-thumbs-up','zz-hand-victory-hand','zzzz-baby','zzzz-eyes','zzzz-robot','zzzz-skull-and-crossbones','zzzz_alien','yellow-2','yellow-3','yellow-5','yellow-1','yellow-6','yellow-34','yellow-35','yellow-36','yellow-37','yellow-38','yellow-41'];
        var food = ['avocado','bacon','banana','beer-mug','birthday-cake','bottle-with-popping-cork','cherries','coconut','cookie','cup-with-straw','cupcake','cut-of-meat','doughnut','eggplant','hamburger','hot-beverage','hot-dog','hot-pepper','ice-cream','lemon','pancakes','peach','pie','pineapple','pizza','poultry-leg','red-apple','sandwich','shaved-ice','soft-ice-cream','strawberry','sushi','taco','tangerine','tropical-drink','tumbler-glass','watermelon','wine-glass'];
        var nature = ['cherry-blossom','christmas-tree','cow-face','crescent-moon','deciduous-tree','fish','frog','hibiscus','monkey-face','mouse-face','pig-face','rabbit-face','rose','sloth','spouting-whale','sun','tulip','unicorn','zzzz-dog-face','zzzz-panda'];
        var objects = ['airplane','alarm-clock','american-football','automobile','baby-bottle','balloon','baseball','basketball','bicycle','blue-book','bomb','bone','camera','cloud','collision','crown','desktop-computer','fire','footprints','gem-stone','ghost','house','key','laptop-computer','light-bulb','lollipop','magnet','mobile-phone','money-bag','money-with-wings','party-popper','pencil','pool-8-ball','rainbow','red-heart','snowman-without-snow','soccer-ball','sunglasses','sweat-droplets','teddy-bear','television','wrapped-gift','zzzz-jack-o-lantern'];
        var realTextField = this;
        var elemParent = realTextField.parent();

        var methods = {
            insertAtCaret: function(node){
                var sel = blurSelection;
                if (sel.rangeCount) {

                    var range = sel.getRangeAt(0);
                    range.collapse(false);
                    range.insertNode(node);
                    range.collapseAfter(node);
                    range.collapse(false);
                    sel.setSingleRange(range);
                }
            }
        };

        /*var mainContainer = $('<div class="atm_emojis_main_container"/>');
        mainContainer.append(realTextField);
        mainContainer.append('<div style="clear:both" />');
        elemParent.append(mainContainer);*/

        //MAIN BUTTON TO OPEN THE EMOJIS LISTS
        var emojisMenu = $('<div class="atm_emojis_menu" />').append('<span class="atm_emojis_open_menu">😀</span>');
        elemParent.append(emojisMenu);

        //CONTAINER THAT WILL HAVE ALL THE EMOJIS TABS AND LISTS
        var emojisContainer = $('<div class="atm_emojis_container" style="display:none"/>');

        //EMOJIS TABS
        var emojisTabs = $('<ul class="atm_emojis_tabs" id="emojisTabs" style="display:none"/>');
        emojisTabs.append('<li class="atm_emojis_tab atm_emojis_tab_selected" data-emojis="yellow"><i class="emojis-sprite grinning-face" style="vertical-align: middle;"></i></li>');
        emojisTabs.append('<li class="atm_emojis_tab" data-emojis="blue"><i class="emojis-sprite blue-153" style="vertical-align: middle;"></i></li>');
        emojisTabs.append('<li class="atm_emojis_tab" data-emojis="cyan"><i class="emojis-sprite cyan-153" style="vertical-align: middle;"></i></li>');
        emojisTabs.append('<li class="atm_emojis_tab" data-emojis="grey"><i class="emojis-sprite grey-153" style="vertical-align: middle;"></i></li>');
        emojisTabs.append('<li class="atm_emojis_tab" data-emojis="pink"><i class="emojis-sprite pink-153" style="vertical-align: middle;"></i></li>');
        emojisTabs.append('<li class="atm_emojis_tab" data-emojis="rose"><i class="emojis-sprite rose-153" style="vertical-align: middle;"></i></li>');
        emojisTabs.append('<li class="atm_emojis_tab" data-emojis="silver"><i class="emojis-sprite silver-153" style="vertical-align: middle;"></i></li>');
        emojisTabs.append('<li class="atm_emojis_tab" data-emojis="food"><i class="emojis-sprite hamburger" style="vertical-align: middle;"></i></li>');
        emojisTabs.append('<li class="atm_emojis_tab" data-emojis="nature"><i class="emojis-sprite rabbit-face" style="vertical-align: middle;"></i></li>');
        emojisTabs.append('<li class="atm_emojis_tab" data-emojis="objects"><i class="emojis-sprite light-bulb" style="vertical-align: middle;"></i></li></li>');
        emojisTabs.append('<li class="atm_emojis_tab_close"><i class="fas fa-times"></i></li>');
        elemParent.append(emojisTabs);


        //CREATION OF ALL THE EMOJIS LISTS
        var yellowList = $('<ul class="atm_emojis_yellow atm_emojis_open" />');
        $.each(yellow,function(index,item){
            yellowList.append('<li data-atm-emoji-icon="emojis-sprite '+item+'" class="atm_emojis_icon"><i class="emojis-sprite ' + item + '"></i></li>');
        });
        emojisContainer.append(yellowList);

        var blueList = $('<ul class="atm_emojis_blue" style="display:none"/>');
        $.each(blue,function(index,item){
            blueList.append('<li data-atm-emoji-icon="emojis-sprite '+item+'" class="atm_emojis_icon"><i class="emojis-sprite ' + item + '"></i></li>');
        });
        emojisContainer.append(blueList);

        var cyanList = $('<ul class="atm_emojis_cyan" style="display:none"/>');
        $.each(cyan,function(index,item){
            cyanList.append('<li data-atm-emoji-icon="emojis-sprite '+item+'" class="atm_emojis_icon"><i class="emojis-sprite ' + item + '"></i></li>');
        });
        emojisContainer.append(cyanList);

        var greyList = $('<ul class="atm_emojis_grey" style="display:none"/>');
        $.each(grey,function(index,item){
            greyList.append('<li data-atm-emoji-icon="emojis-sprite '+item+'" class="atm_emojis_icon"><i class="emojis-sprite ' + item + '"></i></li>');
        });
        emojisContainer.append(greyList);

        var pinkList = $('<ul class="atm_emojis_pink" style="display:none"/>');
        $.each(pink,function(index,item){
            pinkList.append('<li data-atm-emoji-icon="emojis-sprite '+item+'" class="atm_emojis_icon"><i class="emojis-sprite ' + item + '"></i></li>');
        });
        emojisContainer.append(pinkList);

        var roseList = $('<ul class="atm_emojis_rose" style="display:none"/>');
        $.each(rose,function(index,item){
            roseList.append('<li data-atm-emoji-icon="emojis-sprite '+item+'" class="atm_emojis_icon"><i class="emojis-sprite ' + item + '"></i></li>');
        });
        emojisContainer.append(roseList);

        var silverList = $('<ul class="atm_emojis_silver" style="display:none"/>');
        $.each(silver,function(index,item){
            silverList.append('<li data-atm-emoji-icon="emojis-sprite '+item+'" class="atm_emojis_icon"><i class="emojis-sprite ' + item + '"></i></li>');
        });
        emojisContainer.append(silverList);

        var foodList = $('<ul class="atm_emojis_food" style="display:none"/>');
        $.each(food,function(index,item){
            foodList.append('<li data-atm-emoji-icon="emojis-sprite '+item+'" class="atm_emojis_icon"><i class="emojis-sprite ' + item + '"></i></li>');
        });
        emojisContainer.append(foodList);

        var natureList = $('<ul class="atm_emojis_nature" style="display:none"/>');
        $.each(nature,function(index,item){
            natureList.append('<li data-atm-emoji-icon="emojis-sprite '+item+'" class="atm_emojis_icon"><i class="emojis-sprite ' + item + '"></i></li>');
        });
        emojisContainer.append(natureList);

        var objectsList = $('<ul class="atm_emojis_objects" style="display:none"/>');
        $.each(objects,function(index,item){
            objectsList.append('<li data-atm-emoji-icon="emojis-sprite '+item+'" class="atm_emojis_icon"><i class="emojis-sprite ' + item + '"></i></li>');
        });
        emojisContainer.append(objectsList);

        elemParent.append(emojisContainer);

        //STORE THE SELECTION BEFORE CLICKING THE BUTTON THAT SHOW ALL THE EMOJIS TO SAVE THE CARET POSITION
        var blurSelection;
        if(navigator.userAgent.toLowerCase().indexOf('firefox') > -1){
            realTextField.keypress(function(){
                blurSelection = rangy.getSelection();
            });
            realTextField.focus(function(){
                blurSelection = rangy.getSelection();
            });
        }else{
            realTextField.blur(function(){
                blurSelection = rangy.getSelection();
            });
        }

        //MAIN BUTTON THAT SHOWS THE TABS AND THE EMOJIS LISTS
        elemParent.on('click','.atm_emojis_menu',function(){
            if(blurSelection !== undefined){
                var blurRange = blurSelection.getRangeAt(0);
                var sel = rangy.getSelection();
                sel.setSingleRange(blurRange);
            }else{
                realTextField.focus();
                blurSelection = rangy.getSelection();
            }

            var button = $(this);
            button.siblings('.atm_emojis_container,.atm_emojis_tabs').show().addClass('atm_emojis_container_open');
            button.hide();
        });

        //CLOSE BUTTON
        elemParent.on('click','.atm_emojis_tab_close',function(){
            elemParent.find('.atm_emojis_tabs,.atm_emojis_container').hide().removeClass('atm_emojis_container_open');
            elemParent.find('.atm_emojis_menu').show();
            elemParent.find('.emojis_open').hide().removeClass('atm_emojis_open').hide();
            $('#atm_emojis_smileys').addClass('atm_emojis_open').show();
        });

        //TABS THAT SHOW IT'S RESPECTIVE LISTS
        elemParent.on('click','.atm_emojis_tab',function(){
            var button = $(this);
            var listId = 'atm_emojis_' + button.attr('data-emojis');
            var list = elemParent.find('.'+listId);

            elemParent.find('.atm_emojis_open').hide().removeClass('atm_emojis_open');
            $('.atm_emojis_tab_selected').removeClass('atm_emojis_tab_selected');
            if(!list.hasClass('atm_emojis_open')){
                list.addClass('atm_emojis_open').show();
                button.addClass('atm_emojis_tab_selected');
            }
        });

        //INSERT EMOJI IN TEXT FIELD
        elemParent.on('click','.atm_emojis_icon',function(){
            var button = $(this);
            var emoji = document.createElement('span');
            emoji.setAttribute('contenteditable',false);
            emoji.className = button.attr('data-atm-emoji-icon');
            emoji.style.verticalAlign= 'middle';
            methods.insertAtCaret(emoji);
        });

        //CLOSE THE EMOJIS BOX IF THE USER CLICKS OUTSIDE
        $(document).click(function() {
            $('.atm_emojis_tab_close').click();
        });
        elemParent.click(function(event){
            event.stopPropagation();
        });
    }
})(window, jQuery);